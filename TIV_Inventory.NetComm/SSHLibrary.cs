﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chilkat;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace TIV_Inventory.NetComm
{
    public static class SSHLibrary
    {
        private static string homenetDataFile { get; set; }
        private static string chromeNewDataFile { get; set; }
        private static string chromeUsedDataFile { get; set; }

        private static string chilkatKey { get; set; }

        private static string remoteHostname { get; set; }
        private static int remotePort { get; set; }
        private static string remotePath { get; set; }
        private static string localPath { get; set; }

        private static string remoteUsername { get; set; }
        private static string remotePassword { get; set; }

        private static int connectTimeoutMs { get; set; }
        private static int idleTimeoutMs { get; set; }


        static SSHLibrary()
        {
            homenetDataFile = ConfigurationManager.AppSettings["homenetDataFile"];
            chromeNewDataFile = ConfigurationManager.AppSettings["chromeNewDataFile"];
            chromeUsedDataFile = ConfigurationManager.AppSettings["chromeUsedDataFile"];
            
            chilkatKey = ConfigurationManager.AppSettings["ChilkatKey"];

            remoteHostname = ConfigurationManager.AppSettings["remoteHostname"];
            remotePort = Convert.ToInt32(ConfigurationManager.AppSettings["remotePort"]);
            remotePath = ConfigurationManager.AppSettings["remotePath"];
            localPath = ConfigurationManager.AppSettings["localPath"];

            remoteUsername = ConfigurationManager.AppSettings["remoteUsername"];
            remotePassword = ConfigurationManager.AppSettings["remotePassword"];

            connectTimeoutMs = Convert.ToInt32(ConfigurationManager.AppSettings["connectTimeoutMs"]);
            idleTimeoutMs = Convert.ToInt32(ConfigurationManager.AppSettings["idleTimeoutMs"]);

        }
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        public static int RetrieveFiles()
        {
            var result = 0;
            var sw = new Stopwatch();
            sw.Start();

            //var homenetDataFile = "homenet-gaa_data.txt";
            //var chromeNewDataFile = "chromedata-new_english.zip";
            //var chromeUsedDataFile = "chromedata-used_english.zip";

            var filesize = 0L;

            try
            {
                var sftp = new Chilkat.SFtp();

                log.Info("Application Started");

                bool success;
                success = sftp.UnlockComponent(chilkatKey);
                if (success != true)
                {
                    Console.WriteLine(sftp.LastErrorText);
                    log.Error("Chilkat License Verification Failure : " + sftp.LastErrorText);
                    return result;
                }
                else
                {
                    Console.WriteLine(sftp.LastErrorText);
                }

                //  Set some timeouts, in milliseconds:
                sftp.ConnectTimeoutMs = connectTimeoutMs;
                sftp.IdleTimeoutMs = idleTimeoutMs;

                int port;
                string hostname;
                hostname = remoteHostname;
                port = remotePort;
                success = sftp.Connect(hostname, port);
                if (success != true)
                {
                    Console.WriteLine(sftp.LastErrorText);
                    return result;
                }

                log.Info("Connected to " + remoteHostname + ":" + remotePort);

                success = sftp.AuthenticatePw(remoteUsername, remotePassword);
                if (success != true)
                {
                    Console.WriteLine(sftp.LastErrorText);
                    log.Error("Authentication Failure");
                    return result;
                }

                log.Info("Successfuly authenticated");
                
                //  After authenticating, the SFTP subsystem must be initialized:
                success = sftp.InitializeSftp();
                if (success != true)
                {
                    Console.WriteLine(sftp.LastErrorText);
                    log.Error("Initialization Failure");
                    return result;
                }

                log.Info("Successfully Initialized sFTP");

                log.Info("Testing for Pre-existing files");

                if (ValidateFile(homenetDataFile))
                {
                    log.Info("Homenet Data File already exists in destination - Attempting retrieval anyway");
                }

                if (ValidateFile(chromeNewDataFile))
                {
                    log.Info("Chrome New Data File already exists in destination - Attempting retrieval anyway");
                }

                if (ValidateFile(chromeUsedDataFile))
                {
                    log.Info("Chrome Used Data File already exists in destination - Attempting retrieval anyway");
                }


                Debug.WriteLine("Ready to start download");

                #region HomenetDataFile
                var homenetFileStatus = DownloadFile(ref sftp, remotePath + homenetDataFile, localPath + homenetDataFile, out filesize);


                if (homenetFileStatus)
                {
                    log.Info("Successfully retrieved " + homenetDataFile + " in " + sw.ElapsedMilliseconds + "ms " + filesize / sw.ElapsedMilliseconds + " bytes/sec");
                    result++;
                }
                else
                {
                    log.Error("Failed to retrieve " + homenetDataFile);
                }

                Debug.WriteLine(homenetFileStatus
                                      ? "Successfully retrieved homenet-gaa_data.txt in " + sw.ElapsedMilliseconds + "ms " + filesize / sw.ElapsedMilliseconds + " bytes/sec"
                                      : "");

                sw.Restart();
                #endregion

                #region ChromeNewDataFile
                var chromeNewFileStatus = DownloadFile(ref sftp, remotePath + chromeNewDataFile, localPath + chromeNewDataFile, out filesize);

                if (chromeNewFileStatus)
                {
                    log.Info("Successfully retrieved " + chromeNewDataFile + " in " + sw.ElapsedMilliseconds + "ms " + filesize / sw.ElapsedMilliseconds + " bytes/sec");
                    result++;
                }
                else
                {
                    log.Error("Failed to retrieve " + chromeNewDataFile);
                }


                Debug.WriteLine(chromeNewFileStatus
                                      ? "Successfully retrieved " + chromeNewDataFile + " " + sw.ElapsedMilliseconds + "ms " + filesize / sw.ElapsedMilliseconds + "bytes/sec"
                                      : "Failed to retrieve " + chromeNewDataFile);

                sw.Restart(); 
                #endregion


                #region ChromeUsedDataFile
                var chromeUsedFileStatus = DownloadFile(ref sftp, remotePath + chromeUsedDataFile, localPath + chromeUsedDataFile, out filesize);

                if (chromeUsedFileStatus)
                {
                    log.Info("Successfully retrieved " + chromeUsedDataFile + " in " + sw.ElapsedMilliseconds + "ms " + filesize / sw.ElapsedMilliseconds + " bytes/sec");
                    result++;
                }
                else
                {
                    log.Error("Failed to retrieve " + chromeUsedDataFile);
                }

                Debug.WriteLine(chromeUsedFileStatus
                                      ? "Successfully retrieved " + chromeUsedDataFile + " " + sw.ElapsedMilliseconds + "ms " + filesize / sw.ElapsedMilliseconds + "bytes/sec"
                                      : "Failed to retrieve " + chromeUsedDataFile);

                #endregion

                log.Info("Validating retrieved files");

                if (ValidateFile(homenetDataFile))
                {
                    log.Info("Homenet Data File found in destination");
                }

                if (ValidateFile(chromeNewDataFile))
                {
                    log.Info("Chrome New Data File found in destination");
                }

                if (ValidateFile(chromeUsedDataFile))
                {
                    log.Info("Chrome Used Data File found in destination");
                }
            }
            catch (Exception ex)
            {
                log.Error("General Exception", ex);
            }

            log.Info("Application Completed");

            return result;
        }

        private static bool DownloadFile(ref Chilkat.SFtp sftp, string filenameSourcePath, string filenameDestinationPath, out long filesize)
        {
            var result = false;
            filesize = 0L;
            try
            {
                //  Open a file on the server:
                string handle;
                handle = sftp.OpenFile(filenameSourcePath, "readOnly", "openExisting");

                filesize = sftp.GetFileSize64(filenameSourcePath, true, false);
                if (handle == null)
                {
                    Console.WriteLine(sftp.LastErrorText);
                    result = false;
                    return result;
                }

                //  Download the file:
                result = sftp.DownloadFile(handle, filenameDestinationPath);
                if (result != true)
                {
                    Console.WriteLine(sftp.LastErrorText);
                    result = false;
                    return result;
                }
                
                //  Close the file.
                result = sftp.CloseHandle(handle);
                if (result != true)
                {
                    Console.WriteLine(sftp.LastErrorText);
                    result = false;
                }

                if (File.Exists(filenameDestinationPath))
                {
                    result = true;
                }
                
            }
            catch (Exception ex)
            {

                result = false;
            }

            return result;
        }
        
        private static bool ValidateFile(string filenameDestinationPath)
        {
            var result = false;

            try
            {
                if (File.Exists(localPath + filenameDestinationPath))
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

    }
}
