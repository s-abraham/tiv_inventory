﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIV_Inventory.NetComm;

namespace TIV_Inventory.Console
{
    class Program
    {
        static int Main(string[] args)
        {
            var result = 0;

            result = SSHLibrary.RetrieveFiles();
            Environment.Exit(result);

            return result;
        }
    }
}
